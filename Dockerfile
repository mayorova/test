FROM registry.access.redhat.com/openshift3/jenkins-slave-base-rhel7:v3.10

USER root

RUN yum-config-manager --enable rhel-7-server-extras-rpms \
    && yum-config-manager --enable rhel-server-rhscl-7-rpms

# Install Node.JS
# from https://github.com/redhat-cop/containers-quickstarts/tree/master/jenkins-slaves/jenkins-slave-npm

ENV NODEJS_VERSION=8 \
    NPM_CONFIG_PREFIX=$HOME/.npm-global \
    PATH=$HOME/node_modules/.bin/:$HOME/.npm-global/bin/:$PATH \
    BASH_ENV=/usr/local/bin/scl_enable \
    ENV=/usr/local/bin/scl_enable \
    PROMPT_COMMAND=". /usr/local/bin/scl_enable"

COPY contrib/bin/scl_enable /usr/local/bin/scl_enable

 RUN INSTALL_PKGS="rh-nodejs${NODEJS_VERSION} rh-nodejs${NODEJS_VERSION}-npm rh-nodejs${NODEJS_VERSION}-nodejs-nodemon nss_wrapper" && \
    ln -s /usr/lib/node_modules/nodemon/bin/nodemon.js /usr/bin/nodemon && \
    yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y

RUN chown -R 1001:0 $HOME && \
    chmod -R g+rw $HOME

# Install newman
RUN bash -c "npm install newman"

# RUN source scl_source enable python2
# RUN scl enable python2 bash

USER 1001
